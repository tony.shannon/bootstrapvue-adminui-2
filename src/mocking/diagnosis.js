export const DIAGNOSIS = {
    "id": "00ca6980-ec64-424f-b7a7-deb863ec738e",
    "content": {
        "c0001": [{
            "id": "f88f2e5c-61b5-44b9-a581-8a2d55d2ebb4",
            "severity": {"term": "severity/mild", "rubric": "Mild"},
            "clin_descrip": "pain in joints",
            "problem_diagnosis_name": {"term": "problem_diagnosis_names/Arthritis", "rubric": "Arthritis "}
        }, {
            "id": "f33354b5-b937-4fed-ae7e-dd5a4e3dd2f9",
            "severity": {"term": "severity/mod", "rubric": "Moderate"},
            "clin_descrip": "weak hair",
            "problem_diagnosis_name": {"term": "problem_diagnosis_names/alopecia areata", "rubric": "Alopecia areata"}
        }, {
            "id": "e9a08fbf-5b4d-45ad-9af2-5b6607a8f45a",
            "severity": {"term": "severity/severe", "rubric": "Severe"},
            "clin_descrip": "brittle diabetes with unstable blood sugars",
            "problem_diagnosis_name": {"term": "problem_diagnosis_names/diabetes", "rubric": "Diabetes"}
        }]
    },
    "age_hours": 0.04028426583333333,
    "age_days": 0,
    "created_by_name": "Tony Shannon",
    "updated_by_name": "Tony Shannon",
    "edcevent_id": "d2eb6bf7-b3e7-4f28-86b3-e9138a17e8eb",
    "edcevent_type": "d6cde447-9726-4130-8b91-33f0538a2212",
    "data_verified": false,
    "data_verified_date": null,
    "data_verification": null,
    "data_verification_pct": null,
    "pathway": "1e1ce6f0-3504-43e7-8f4f-0374801323a9",
    "label": "Diagnosis_form4",
    "frozen": false,
    "locked": false,
    "sdv_enabled": false,
    "created_date": "2020-05-11T16:06:11.791269+01:00",
    "updated_date": null,
    "event_date": "2020-05-11T12:00:00+01:00",
    "state": 1,
    "etl_state": 0,
    "external_id": null,
    "deleted": false,
    "track_changes": false,
    "rag": "G",
    "section_rag": {"Main": "G"},
    "concept_rag": {"c0001": ["G", "G", "G"]},
    "variations": "",
    "ui_state": {},
    "percent_complete": null,
    "record": "57962105-27e7-421a-9007-54f738f1d347",
    "template": "40ce24b6-1c28-44f3-bd1a-bd9a759a7d33",
    "created_by": 745543738,
    "updated_by": 745543738,
    "owned_by": 946648253,
    "study": "67385377-9514-4104-b6c3-27d20a79132b",
    "wf_state": "Open",
    "transitions": [["Frozen", "Freeze", null]],
    "actions": ["Edit", "View"]
}
