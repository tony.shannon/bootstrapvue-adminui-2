const obj = {
    "data": {
        "all_enrolments": [{
            "created_date": "2020-03-03T12:45:56.735925+00:00",
            "patient_id": "123",
            "record": {
                "first_name": "Tony",
                "family_name": "Murphy",
                "date_of_birth": "2020-03-01",
                "gender": "Male",
                "id": "9fbf05cb-98fa-4f66-ac54-63ef47b3f92c"
            }
        }, {
            "created_date": "2020-03-03T15:10:39.376242+00:00",
            "patient_id": "345",
            "record": {
                "first_name": "John",
                "family_name": "McMorrow",
                "date_of_birth": "1960-02-03",
                "gender": "Male",
                "id": "57962105-27e7-421a-9007-54f738f1d347"
            }
        }]
    }
};

export const PATIENT_LIST = obj;

